import sys
import data
import circos
import coverage
import karyogram
import heatmap
from PySide.QtCore import *
from PySide.QtGui import *

#The main window of the program. Handles a central view widget, and menus and toolbars.
class WGSView(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initmainwin()
        self.activeScene = False

    def initmainwin(self):
        self.setWindowTitle('WGS')
        self.resize(800,600)
        #Center the main window on the user's screen
        frameGeo = self.frameGeometry()
        desktopCenter = QDesktopWidget().availableGeometry().center()
        frameGeo.moveCenter(desktopCenter)
        self.move(frameGeo.topLeft())
        #Adds a status bar to the main window
        self.statusBar()
        #Create actions for menus and toolbars, connect to functions
        self.newCircAct = QAction('New CIRCOS',self)
        self.newCircAct.triggered.connect(self.newCirc)
        self.newCovDiagramAct = QAction('New coverage diagram',self)
        self.newCovDiagramAct.triggered.connect(self.newCovDiagram)
        self.newKaryogramAct = QAction('New karyogram',self)
        self.newKaryogramAct.triggered.connect(self.newKaryogram)
        self.newHeatmapAct = QAction('New heatmap',self)
        self.newHeatmapAct.triggered.connect(self.newHeatmap)
        self.exitAct = QAction('&Exit',self)
        self.exitAct.triggered.connect(self.close)
        #Create menus, toolbar, and add actions
        self.menubar = self.menuBar()
        self.fileMenu = self.menubar.addMenu('&File')
        self.fileMenu.addAction(self.newCircAct)
        self.fileMenu.addAction(self.newCovDiagramAct)
        self.fileMenu.addAction(self.newKaryogramAct)
        self.fileMenu.addAction(self.newHeatmapAct)
        self.fileMenu.addAction(self.exitAct)
        self.show()

    #Creates and initializes a new circos diagram
    def newCirc(self):
        startNew = True
        if self.activeScene:
            newSceneDialog = QDialog()
            newSceneDialog.setWindowTitle("Are you sure?")
            okButton = QPushButton('Ok', newSceneDialog)
            okButton.clicked.connect(newSceneDialog.accept)
            cancelButton = QPushButton('Cancel', newSceneDialog)
            cancelButton.clicked.connect(newSceneDialog.reject)
            textLabel = QLabel("The current scene will be lost. Are you sure?")
            newSceneDialog.layout = QGridLayout(newSceneDialog)
            newSceneDialog.layout.addWidget(textLabel,0,0,1,2)
            newSceneDialog.layout.addWidget(okButton,1,0)
            newSceneDialog.layout.addWidget(cancelButton,1,1)
            choice = newSceneDialog.exec_()
            if choice == QDialog.Accepted:
                startNew = True
            else:
                startNew = False
        if startNew:

            #Remove old toolbars, menu items, clear up resources
            try:
                self.removeToolBar(self.tools)
                self.tools.hide()
                self.tools.destroy()
                self.view.destroy()
            except:
                pass
            self.fileMenu.clear()

            self.scene = circos.CircosScene(self)
            self.view = circos.CircosView(self.scene)
            self.setCentralWidget(self.view)

            self.viewSettingsAct = QAction('&Settings',self)
            self.viewSettingsAct.triggered.connect(self.view.viewSettings)
            self.exportImageAct = QAction('Export image',self)
            self.exportImageAct.triggered.connect(self.exportImage)
            self.fileMenu.addAction(self.newCircAct)
            self.fileMenu.addAction(self.newCovDiagramAct)
            self.fileMenu.addAction(self.newKaryogramAct)
            self.fileMenu.addAction(self.newHeatmapAct)
            self.fileMenu.addAction(self.viewSettingsAct)
            self.fileMenu.addAction(self.exportImageAct)
            self.fileMenu.addAction(self.exitAct)
            self.showChInfoAct = QAction('&Chromosomes',self)
            self.showChInfoAct.triggered.connect(self.view.showChInfo)
            self.updateSceneAct = QAction('&Update CIRCOS',self)
            self.updateSceneAct.triggered.connect(self.updateScene)
            self.toggleCoverageAct = QAction('&Toggle coverage',self)
            self.toggleCoverageAct.triggered.connect(self.view.toggleCoverage)
            self.addImageAct = QAction('&Add Image to plot', self)
            self.addImageAct.triggered.connect(self.view.addImage)
            self.importColorTabAct = QAction('&Color regions with file', self)
            self.importColorTabAct.triggered.connect(self.view.importColorTab)
            self.tools = self.addToolBar('Chromosome tools')
            self.tools.addAction(self.showChInfoAct)
            self.tools.addAction(self.updateSceneAct)
            self.tools.addAction(self.toggleCoverageAct)
            self.tools.addAction(self.addImageAct)
            self.tools.addAction(self.importColorTabAct)

            #Some confusion with python bindings makes getOpenFileName return a tuple.
            #First element is the name of the file.
            self.statusBar().showMessage("Initializing new CIRCOS..")
            #Null string if cancel is pressed -- FIX: handle.
            tabFile = QFileDialog.getOpenFileName(None,"Specify TAB file",QDir.currentPath(),
            "TAB files (*.tab)")[0]
            vcfFile = QFileDialog.getOpenFileName(None,"Specify VCF file",QDir.currentPath(),
            "VCF files (*.vcf)")[0]
            self.statusBar().showMessage("Reading TAB..")
            self.reader = data.Reader()
            self.reader.readTab(tabFile)
            self.statusBar().showMessage("Reading VCF..")
            self.reader.readVCF(vcfFile)

            self.view.chromosomes = self.reader.returnChrList()
            self.view.numChr = len(self.view.chromosomes)
            self.view.coverageNormLog = self.reader.returnCoverageNormLog()
            self.view.coverageNorm = self.reader.returnCoverageNorm()
            self.view.tabName = self.reader.returnTabName()
            self.view.vcfName = self.reader.returnVcfName()
            self.view.addFileText()
            self.view.createChInfo()
            self.tools.show()
            self.view.chromosomeItems = []
            #Create a dict representing colors for the 24 default chromosomes
            self.view.chromoColors = {}
            color = self.view.startColor
            for i in range(24):
                self.view.chromoColors[self.view.chromosomes[i].name] = color
                color = color.darker(105)
            self.updateScene()
            self.activeScene = True
            self.show()
            self.view.showChInfo()

    #Method for updating the circos view
    def updateScene(self):
        self.statusBar().showMessage("Drawing..")
        self.view.initscene()
        self.statusBar().clearMessage()

    #Exports anything in the current view as a png image
    def exportImage(self):
        #Set default name to same as vcf file if this is loaded, otherwise use tab
        try:
            defaultPath = QDir.currentPath() + "/" + self.reader.returnVcfName()
            defaultPath = defaultPath.replace("vcf","png")
        except:
            defaultPath = QDir.currentPath() + "/" + self.reader.returnTabName()
            defaultPath = defaultPath.replace("tab","png")
        savePath = QFileDialog.getSaveFileName(self, "Export image", defaultPath, "Images (*.png)")[0]
        viewPixMap = QPixmap.grabWidget(self.view)
        viewPixMap.save(savePath)

    #Creates and initializes a new coverage diagram
    def newCovDiagram(self):
        startNew = True
        if self.activeScene:
            newSceneDialog = QDialog()
            newSceneDialog.setWindowTitle("Are you sure?")
            okButton = QPushButton('Ok', newSceneDialog)
            okButton.clicked.connect(newSceneDialog.accept)
            cancelButton = QPushButton('Cancel', newSceneDialog)
            cancelButton.clicked.connect(newSceneDialog.reject)
            textLabel = QLabel("The current scene will be lost. Are you sure?")
            newSceneDialog.layout = QGridLayout(newSceneDialog)
            newSceneDialog.layout.addWidget(textLabel,0,0,1,2)
            newSceneDialog.layout.addWidget(okButton,1,0)
            newSceneDialog.layout.addWidget(cancelButton,1,1)
            choice = newSceneDialog.exec_()
            if choice == QDialog.Accepted:
                startNew = True
            else:
                startNew = False
        if startNew:

            #Remove old toolbars, menu items, clear up resources
            try:
                self.removeToolBar(self.tools)
                self.tools.hide()
                self.tools.destroy()
                self.view.destroy()
            except:
                pass
            self.fileMenu.clear()

        self.statusBar().showMessage("Initializing new coverage diagram..")
        tabFile = QFileDialog.getOpenFileName(None,"Specify TAB file",QDir.currentPath(),
        "TAB files (*.tab)")[0]
        self.statusBar().showMessage("Reading TAB..")
        self.reader = data.Reader()
        self.reader.readTab(tabFile)
        self.statusBar().clearMessage()
        self.view = coverage.CoverageView(self.reader.chromosomes)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidget(self.view)
        self.scrollArea.setWidgetResizable(True)
        self.setCentralWidget(self.scrollArea)
        self.view.coverageNormLog = self.reader.returnCoverageNormLog()
        self.view.coverageNorm = self.reader.returnCoverageNorm()

        self.viewSettingsAct = QAction('&Settings',self)
        self.viewSettingsAct.triggered.connect(self.view.viewSettings)
        self.exportImageAct = QAction('Export image',self)
        self.exportImageAct.triggered.connect(self.exportImage)
        self.fileMenu.addAction(self.newCircAct)
        self.fileMenu.addAction(self.newCovDiagramAct)
        self.fileMenu.addAction(self.newKaryogramAct)
        self.fileMenu.addAction(self.newHeatmapAct)
        self.fileMenu.addAction(self.viewSettingsAct)
        self.fileMenu.addAction(self.exportImageAct)
        self.fileMenu.addAction(self.exitAct)
        self.tools = self.addToolBar('Coverage tools')
        self.addPlotAct = QAction('Add subplot', self)
        self.addPlotAct.triggered.connect(self.view.addChromoPlot)
        self.updateLayoutAct = QAction('Update layout', self)
        self.updateLayoutAct.triggered.connect(self.view.arrangePlots)
        self.tools.addAction(self.addPlotAct)
        self.tools.addAction(self.updateLayoutAct)
        self.tools.show()
        self.show()
        self.activeScene = True

    #Creates and initializes a new karyotype diagram
    def newKaryogram(self):
        startNew = True
        if self.activeScene:
            newSceneDialog = QDialog()
            newSceneDialog.setWindowTitle("Are you sure?")
            okButton = QPushButton('Ok', newSceneDialog)
            okButton.clicked.connect(newSceneDialog.accept)
            cancelButton = QPushButton('Cancel', newSceneDialog)
            cancelButton.clicked.connect(newSceneDialog.reject)
            textLabel = QLabel("The current scene will be lost. Are you sure?")
            newSceneDialog.layout = QGridLayout(newSceneDialog)
            newSceneDialog.layout.addWidget(textLabel,0,0,1,2)
            newSceneDialog.layout.addWidget(okButton,1,0)
            newSceneDialog.layout.addWidget(cancelButton,1,1)
            choice = newSceneDialog.exec_()
            if choice == QDialog.Accepted:
                startNew = True
            else:
                startNew = False
        if startNew:

            #Remove old toolbars, menu items, clear up resources
            try:
                self.removeToolBar(self.tools)
                self.tools.hide()
                self.tools.destroy()
                self.view.destroy()
            except:
                pass
            self.fileMenu.clear()

        self.statusBar().showMessage("Initializing karyogram..")
        #Null string if cancel is pressed -- FIX: handle.
        tabFile = QFileDialog.getOpenFileName(None,"Specify TAB file",QDir.currentPath(),
        "TAB files (*.tab)")[0]
        vcfFile = QFileDialog.getOpenFileName(None,"Specify VCF file",QDir.currentPath(),
        "VCF files (*.vcf)")[0]
        cytoFile = QFileDialog.getOpenFileName(None,"Specify cytoband file",QDir.currentPath(),
        "cytotab files (*.txt)")[0]
        self.statusBar().showMessage("Reading TAB..")
        self.reader = data.Reader()
        self.reader.readTab(tabFile)
        self.statusBar().showMessage("Reading VCF..")
        self.reader.readVCF(vcfFile)
        self.statusBar().showMessage("Reading cytoband file..")
        self.reader.readCytoTab(cytoFile)
        self.statusBar().clearMessage()

        self.view = karyogram.KaryogramView(self.reader.chromosomes, self.reader.returnCytoTab())
        self.view.createChInfo()
        self.setCentralWidget(self.view)

        self.viewSettingsAct = QAction('&Settings',self)
        self.viewSettingsAct.triggered.connect(self.view.viewSettings)
        self.exportImageAct = QAction('Export image',self)
        self.exportImageAct.triggered.connect(self.exportImage)
        self.fileMenu.addAction(self.newCircAct)
        self.fileMenu.addAction(self.newCovDiagramAct)
        self.fileMenu.addAction(self.newKaryogramAct)
        self.fileMenu.addAction(self.newHeatmapAct)
        self.fileMenu.addAction(self.viewSettingsAct)
        self.fileMenu.addAction(self.exportImageAct)
        self.fileMenu.addAction(self.exitAct)
        self.tools = self.addToolBar('Karyogram tools')
        self.updateKaryogramAct = QAction('Update karyogram', self)
        self.updateKaryogramAct.triggered.connect(self.view.updateItems)
        self.showChInfoAct = QAction('&Chromosomes',self)
        self.showChInfoAct.triggered.connect(self.view.showChInfo)
        self.tools.addAction(self.showChInfoAct)
        self.tools.addAction(self.updateKaryogramAct)
        self.tools.show()
        self.show()
        self.activeScene = True

    #Creates and initializes a new heatmap diagram
    def newHeatmap(self):
        startNew = True
        if self.activeScene:
            newSceneDialog = QDialog()
            newSceneDialog.setWindowTitle("Are you sure?")
            okButton = QPushButton('Ok', newSceneDialog)
            okButton.clicked.connect(newSceneDialog.accept)
            cancelButton = QPushButton('Cancel', newSceneDialog)
            cancelButton.clicked.connect(newSceneDialog.reject)
            textLabel = QLabel("The current scene will be lost. Are you sure?")
            newSceneDialog.layout = QGridLayout(newSceneDialog)
            newSceneDialog.layout.addWidget(textLabel,0,0,1,2)
            newSceneDialog.layout.addWidget(okButton,1,0)
            newSceneDialog.layout.addWidget(cancelButton,1,1)
            choice = newSceneDialog.exec_()
            if choice == QDialog.Accepted:
                startNew = True
            else:
                startNew = False
        if startNew:

            #Remove old toolbars, menu items, clear up resources
            try:
                self.removeToolBar(self.tools)
                self.tools.hide()
                self.tools.destroy()
                self.view.destroy()
            except:
                pass
            self.fileMenu.clear()

        self.statusBar().showMessage("Initializing new heatmap..")
        tabFile = QFileDialog.getOpenFileName(None,"Specify TAB file",QDir.currentPath(),
        "TAB files (*.tab)")[0]
        vcfFile = QFileDialog.getOpenFileName(None,"Specify VCF file",QDir.currentPath(),
        "VCF files (*.vcf)")[0]
        self.statusBar().showMessage("Reading TAB..")
        self.reader = data.Reader()
        self.reader.readTab(tabFile)
        self.statusBar().showMessage("Reading VCF..")
        self.reader.readVCF(vcfFile)
        self.statusBar().clearMessage()
        self.view = heatmap.HeatmapView(self.reader.chromosomes)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidget(self.view)
        self.scrollArea.setWidgetResizable(True)
        self.setCentralWidget(self.scrollArea)

        self.viewSettingsAct = QAction('&Settings',self)
        self.viewSettingsAct.triggered.connect(self.view.viewSettings)
        self.exportImageAct = QAction('Export image',self)
        self.exportImageAct.triggered.connect(self.exportImage)
        self.fileMenu.addAction(self.newCircAct)
        self.fileMenu.addAction(self.newCovDiagramAct)
        self.fileMenu.addAction(self.newKaryogramAct)
        self.fileMenu.addAction(self.newHeatmapAct)
        self.fileMenu.addAction(self.viewSettingsAct)
        self.fileMenu.addAction(self.exportImageAct)
        self.fileMenu.addAction(self.exitAct)
        self.tools = self.addToolBar('Coverage tools')
        self.addHeatmapAct = QAction('Add heatmap', self)
        self.addHeatmapAct.triggered.connect(self.view.addHeatmap)
        self.updateLayoutAct = QAction('Update layout', self)
        self.updateLayoutAct.triggered.connect(self.view.arrangePlots)
        self.tools.addAction(self.addHeatmapAct)
        self.tools.addAction(self.updateLayoutAct)
        self.tools.show()
        self.show()
        self.activeScene = True
